package com.lez18.hw.GestioneUniversitaria;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestioneUniversitariaApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestioneUniversitariaApplication.class, args);
	}

}
